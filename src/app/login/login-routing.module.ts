import { NgModule } from "@angular/core";
import { RouterModule, Router } from '@angular/router';
import { loginRoutes } from './login-routing';

@NgModule({
    imports: [RouterModule.forChild(loginRoutes)],
    exports: [RouterModule]
})
export class LoginRoutingModule{}