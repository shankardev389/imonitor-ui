import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {layoutRoutes } from './layout-routing';
import { RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { LayoutRoutingModule } from './layout-routing.module';

@NgModule({
  declarations: [LayoutComponent],
  imports: [
     CommonModule,
     LayoutRoutingModule
  ]
})
export class LayoutModule { }
