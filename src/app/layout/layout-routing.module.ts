import { RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { layoutRoutes } from './layout-routing';


@NgModule({
  imports: [RouterModule.forChild(layoutRoutes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule{}