import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { registerRoutes } from './register-routing';

@NgModule({
    imports: [RouterModule.forChild(registerRoutes)],
    exports: [RouterModule]
})
export class RegisterRoutingModule{}