import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'login', 
    loadChildren: () => import('./login/login.module').then(module=>module.LoginModule)
  },
  {
    path: 'register', 
    loadChildren: () => import('./register/register.module').then(module=>module.LoginModule)
  },
  {
    path: 'imonitor', 
    loadChildren: () => import('./layout/layout.module').then(module=>module.LayoutModule)
  },
  { 
    path: '**', redirectTo: 'login', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
